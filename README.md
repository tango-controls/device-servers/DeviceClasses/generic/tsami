# TSAMI (Tango State MAchine Interpreter)

TSAMI is a small set of tools to be able to design and deploy state machines which interact with the tango controls system. 
It consists of a small DSL (Domain Specific Language) which allows for definition of the state machines, an svg generator that uses graphviz to generate a graphical representation of the state machine and a simple webUI to manage all state machines.

## The language

To define a state machine TSAMI uses its own language by the same name. It is a very simple language inspired by EPICS' SNL. Here we will define the following state machine using the tsami language:

![Diagram of the prevoius state machine](/docs/imgs/output.png)

```
name "docs";

assign {
  FRG02_1:DevLong64 "testds/eps/1/X25A0203CH21_VCPEN_EH0103_DI";
  PrimaryPump:DevLong64 "testds/eps/1/VCPP_EH0101";
}

safe_start {
  FRG02_1 is 0;
} goto initial_state

state initial_state {
  entry {
    set PrimaryPump 1;
  }
  when (FRG02_1 == 1) {
    set PrimaryPump 0;
  } goto vacuumed_chamber
  when (delay 600) {
    set PrimaryPump 0;
  } goto error_state
}

state vacuumed_chamber {
  /* In case we lose vacuum start process again */
  when (FRG02_1 == 0) {
  } goto initial_state
}

state error_state {
  entry {
    set PrimaryPump 0;
  }
  /* Right now at least we require one when clause (to be fixed)
     so we make an edge that point to itself after some time. */
  when (delay 900000) {
  } goto error_state
}
```
In this example we define two variables and bind them to tango attributes with their types. There is no typechacking beyond this point
so if inside a comparison the types are wrong it will only be detected at runtime, as of right now it is more of a type hint than
anything else. 

A `safe_start` defines a series of conditions which indicate to the state machine that it is safe to start running.
In case it doesn't pass it just fails and doesn't keep trying until met. This block **must always be defined** but can be left empty, 
that is beacause the final goto statement point to the initial state for the state machine. 

A state is defined by the keyword `state` followed by that state's id which must be unique. 
The state definition may contain an `entry` block which will run anytime the state is reached. 
It also may contain any number of `when` clauses which indicate the exit options of the state. The conditional clause of the `when` clause is written in between partentheses and must always avaluate to either `true` or `false`. This block may contain a number of actions to be preformed before moving to the next state. 
After the `when` block there is a `goto` statement which indicates which state it moves to if that clause is met. 

## Installation

To install you can do it straight with [poetry](https://python-poetry.org/) running the following command inside the project directory:

`poetry install`

Or you can generate a wheel file with:

`poetry build`

And then pip install the file:

`pip3 install dist/[wheel-file].whl`

Ideally this will have to be impoved so that deployment can be automatized with docker containers and/or automated CI to generate the wheel files. 

## The Parts

The system consists right now of two servers. The first one is a [FastAPI](https://fastapi.tiangolo.com/) that controls and runs the state machines, it is completely independent. The other one is the webUI using [Flask](https://flask.palletsprojects.com/en/3.0.x/) and [Htmx](https://htmx.org/) and it allows you to see the graphical representation fo the state machine as well as see into the status of all the running ones and control them. This one depends on the first one.

The general idea is that if you don't like the webUI or would add extra functionallity which for some reason can't be joined with the webUI here, you can do your own client to the main server just interacting with the FastAPI.

To run the main server you first need to define the environment variable `TSAMIS_PATH` which contains all the `.tsm` state machines you want to have available. Then you can run the entrypoint `tsami_servr` to start the server on port `8000`.

Then to start the WebUI you need to run the entrypoint `tsami_webpage` and that should be it. The webpage can be accessed through port `5000`.


## Future improvements
* Fix safe start to use normal boolean operations
* import funcionallity to divide state machine into various files
* Deal with image cache forthe webUI.
* allow multiple sets in one line to make the file less vertical
* Join webpage and SM handler server
* add functionallyity to webpage:
  + more visibility on what is being evaluated
  + report what fails on a safety check fail
  + include tickbox to skip safety check
  + ability to force different initial state
* proper documentation
* Syntax highlight (?) (vscode has an automatic plugin for textx)
