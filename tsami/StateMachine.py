from os.path import dirname
import threading
import textx
import time
import datetime
import logging
from tsami.SMUtils import (
    TangoVariable,
    compare,
    textx_instance,
    SMState,
    Variable,
    VariableTypesAdapter,
)
from tsami.SMImageGen import tsm_to_dot


logging.basicConfig(level=logging.DEBUG)


class StateMachine(object):
    def __init__(self, model):
        """
        TODO: Docs and get actual state object from the forced_initial_state string.

        :param model: a textx object containing the model of the program
        """
        self.proc_thread: threading.Thread = threading.Thread(
            target=self.start_state_machine
        )
        self._stop_event = threading.Event()
        self.model = model
        self.name = model.name.name
        self.states = [s.name for s in model.states]
        self.status = SMState.STANDBY
        self.forced_initial_state: bool = False
        self.override_safety = None
        self._current_state = None
        self._current_state_startTime = time.time()
        self._path_to_tsm = model._tx_filename
        self.variables: dict[str, Variable] = {}

    def get_current_state(self):
        return self._current_state

    def get_status(self):
        return self.status

    def set_current_state(self, value):
        logging.info(f"Updated state to: {value}")
        self._current_state = value

    def get_path(self):
        return self._path_to_tsm

    def is_state_machine_running(self):
        return self.proc_thread.is_alive()

    def get_dot_graph(self):
        if self.status in [SMState.STANDBY, SMState.ASSIGNING, SMState.SAFETYCHECK]:
            # Sate machine not started yet or in state not in state machine
            # Maybe we should create floating nodes around the state machine to
            # color them in case they fall into them such as errors.
            return tsm_to_dot(self.model)
        return tsm_to_dot(self.model, current_state=self._current_state)

    def assign_variables_from_definitions(self):
        """
        Given a textx:tsm_grammar.Assignment object, modify the internal variables dict
        to add/replace with the ones from the assignment model.

        assign_model (textx:tsm_grammar.Assignment): The assignment model which
            from which to take the variable definition
        """
        self.status = SMState.ASSIGNING
        for assignment in self.model.assigns.declarations:
            variable = VariableTypesAdapter.validate_python(
                [{"name": assignment.name, "content": assignment.content}]
            )[0]

            self.variables[assignment.name] = variable
            # if not tango_attribute_expr.match(assignment.content):
            #     # it it isn't a tango attribute
            #     self.variables[assignment.name] = Variable(assignment.name,
            #                                                "")
            # try:
            #     tmp_proxy: AttributeProxy = AttributeProxy(assignment.content)
            # except Exception as e:
            #     print(e)
            #     raise ConnectionError("Couldt connect to tango")
            # attr_type = tmp_proxy.get_config().data_type
            # try:
            #     assert (CmdArgType.values[attr_type].name == assignment.type)
            # except AssertionError:
            #     raise TypeError(f"Type of {assignment.name}:{assignment.type} is different " +
            #                     "from the Tango Attribute" +
            #                     f"which is {CmdArgType.values[attr_type].name}")

    def run_safety(self):
        """
        given a textx:tsm_grammar.SafeStart model make sure that all the checks pass.

            Raises:
                AssertionError: If any check isn't satisified.
        """
        self.status = SMState.SAFETYCHECK
        logging.info(f"Starting Safety check. Time: {datetime.datetime.now().time()}")
        for check in self.model.safety_check.checks:
            try:
                read_val = self.read_variable(check.variable.name)
                assert read_val == check.value
            except AssertionError:
                self.status = SMState.SAFETYERROR
                raise RuntimeError(
                    "Current state of system does not pass safety check becuase"
                    + f"var {check.variable.name} is {self.read_variable(check.variable.name)}"
                    + f"instead of {check.value}"
                )

        logging.info(f"Finished Safety check. Time: {datetime.datetime.now().time()}")
        return self.model.safety_check.to_state

    def run_actions(self, tangoOperation_model):
        # For now everything is a set operation so this will be kept simple but
        # ideally it will be expanded to be able to run methods and other things
        for operation in tangoOperation_model:
            variable = operation.variable.name
            value = operation.x
            self.write_tango(variable, value)
            logging.debug(f"Wrote {value} to variable {variable}")

    def read_variable(self, variable_name):
        var = self.variables[variable_name]
        return var.read()

    def write_tango(self, variable_name, value):
        try:
            self.variables[variable_name].write(value)
        except Exception as e:
            raise ConnectionError(
                f"Could not write attribute {variable_name.name}. Error: {e}"
            )

    def is_condition_true(self, condition):
        """
        This is the recursive function meant to check the boolens in the WhenClauses.

            Args:
                condition (model that is or derives from OrBoolExp): Boolean expression
                    to check recurssively
        """
        match textx_instance(condition):
            case "Delay":
                current_time = time.time()
                return (current_time - self._current_state_startTime) > condition.time

            # Solve now for simple binary operator
            # Base case for recursive boolean solving
            case "Leaf":
                if condition.variable is not None:
                    return self.read_variable(condition.variable.name)
                else:
                    return condition.x

            case "OrBoolExp":
                if condition.delay:
                    return self.is_condition_true(condition.delay)

                return (
                    self.is_condition_true(condition.left)
                    if not condition.right
                    else self.is_condition_true(condition.left)
                    or self.is_condition_true(condition.right)
                )

            case "AndBoolExp":
                return (
                    self.is_condition_true(condition.left)
                    if not condition.right
                    else self.is_condition_true(condition.left)
                    and self.is_condition_true(condition.right)
                )

            case "Qatom":
                left = self.is_condition_true(condition.left)
                right = self.is_condition_true(condition.right)

                return compare(left, condition.operation, right)

    def run_state(self, state):
        """
        This function runs state 'state' and returns the state name which the
        WhenClause that has been satisfied is pointing to.

            Args:
                state: State model of the state to run

            Returns:
                textx:tsm_grammar.State: the next state, None if end state
        """
        self.set_current_state(state.name)
        if state.entryOp is not None:
            self.run_actions(state.entryOp.tangoOperations)

        if state.exits is None:
            return None  # if no exits it means final state

        self._current_state_startTime = time.time()

        while not self._stop_event.is_set():
            for check in state.exits:
                if self.is_condition_true(check.condition):
                    self.run_actions(check.tangoOperations)
                    return check.to_state
            time.sleep(0.5)

    def start_state_machine(self):
        self._stop_event = threading.Event()
        try:
            self.assign_variables_from_definitions()
        except ConnectionError:
            self.status = SMState.CONNECTIONERROR
            self._current_state = None
            quit()
        except Exception as e:
            print(e)
            self.status = SMState.ERROR
            self._current_state = None
            quit()

        initial_state = self.model.safety_check.to_state
        if self.forced_initial_state:
            logging.warning(
                f"Initial state overriden, set to {self.forced_initial_state}"
            )
            initial_state = self.forced_initial_state

        if not self.override_safety:
            try:
                self.run_safety()
            except Exception as e:
                self.status = SMState.SAFETYERROR
                self._current_state = None
                quit()

        else:
            logging.warning("SAFETY CHECK OVERRIDEN")

        logging.info("Starting Tango State Machine")
        self.status = SMState.RUNNING

        try:
            next_state = self.run_state(initial_state)
            while next_state is not None and not self._stop_event.is_set():
                next_state = self.run_state(next_state)
        except Exception as e:
            self.status = SMState.ERROR
            self._current_state = None
            print(e)

    def stop(self):
        self._stop_event.set()
        self._current_state = None
        self.proc_thread: threading.Thread = threading.Thread(
            target=self.start_state_machine
        )
        self.status = SMState.STANDBY

    def run(self, override_safety=False, forced_initial_state=None):
        """
        Run the state machine that was loaded in the model on creation.
        """
        self.override_safety = override_safety
        self.forced_initial_state = forced_initial_state
        self.proc_thread.start()


if __name__ == "__main__":
    this_folder = dirname(__file__)
    # if len(sys.argv) != 2:
    #     print(f"Usage: python3 {sys.argv[0]} <model>")
    #     quit()

    meta_model = textx.metamodel_from_file(
        "/home/aolle/Documents/test_files/TSAMI/tsm_grammar.tx"
    )
    model = meta_model.model_from_file(
        "/home/aolle/Documents/test_files/TSAMI/minerva_test/start.tsm"
    )
    state_machine = StateMachine(model)
    state_machine.run()
