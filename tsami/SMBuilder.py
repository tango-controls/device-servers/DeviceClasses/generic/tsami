from textx import metamodel_from_file
from tsami.StateMachine import StateMachine
from pathlib import Path


class StateMachineBuilder:
    def __init__(self):
        self._override_safety = None
        self._initial_state = None
        self._metamodel_path: Path = Path(__file__).parent / "tsm_grammar.tx"
        self._model_path: str = ""

    def set_metamodel_path(self, path="../../tsm_grammar.tx"):
        self._metamodel_path = path
        return self

    def set_model_path(self, path):
        self._model_path = path
        return self

    def generate_state_machine(self):
        meta_model = metamodel_from_file(self._metamodel_path.absolute())
        model = meta_model.model_from_file(self._model_path)
        return StateMachine(model)
