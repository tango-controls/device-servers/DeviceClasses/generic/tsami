from dataclasses import dataclass
from pydantic import BaseModel


class CreateTsamiRequest(BaseModel):
    path_to_model: str


class TsamiDesc(BaseModel):
    name: str
    path: str
    states: list[str]
    current_status: str


class AllTsamisResponse(BaseModel):
    machines: list[TsamiDesc]


@dataclass
class GetTsamiParameterRequest:
    parameter: str
