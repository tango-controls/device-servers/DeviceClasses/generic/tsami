import os
from fastapi import FastAPI
import uvicorn

from tsami.SMBuilder import StateMachineBuilder
from tsami.SMUtils import Endpoint
from tsami.server.server_data_types import CreateTsamiRequest

app = FastAPI()
tsamis = {}

builder = StateMachineBuilder()

"""
TODO: Need to do a delete device too
"""


@app.get(Endpoint.ALL_TSAMIS)
async def allTsamis():
    arr = []
    for name, _ in tsamis.items():
        arr.append(name)
    return {"machines": arr}


@app.post(Endpoint.CREATE_TSAMI)
async def generalTsami(request: CreateTsamiRequest):
    print(request.path_to_model)
    buildertsami = StateMachineBuilder().set_model_path(request.path_to_model)

    tmp_sm = buildertsami.generate_state_machine()
    tsamis[tmp_sm.name] = tmp_sm
    return tmp_sm.name


@app.get(Endpoint.RUN_MACHINE)
async def run_machine(name: str):
    try:
        tsamis[name].run()
    except KeyboardInterrupt:
        quit()


@app.get(Endpoint.STOP_MACHINE)
async def stop_machine(name: str):
    tsamis[name].stop()


@app.get(Endpoint.GET_TSAMI_STATE)
async def get_tsami_state(name: str):
    return {"state": tsamis[name].get_current_state()}


@app.get(Endpoint.GET_TSAMI_DOT)
async def get_tsami_dot(name: str):
    return {"dot": tsamis[name].get_dot_graph()}


@app.get(Endpoint.GET_TSAMI_STATUS)
async def get_tsami_status(name: str):
    return {"status": tsamis[name].get_status()}


@app.get(Endpoint.GET_TSAMI_PATH)
async def get_tsami_path(name: str):
    return {"path": tsamis[name].get_path()}


def start():
    path_to_tsamis_files = os.environ["TSAMIS_PATH"]

    if not (path_to_tsamis_files and os.path.exists(path_to_tsamis_files)):
        raise Exception(
            "Environment variable TSAMIS_PATH is not defined or is not a directory."
        )

    for file in os.listdir(path_to_tsamis_files):
        if file.endswith(".tsm"):
            sm = (
                StateMachineBuilder()
                .set_model_path(path_to_tsamis_files + file)
                .generate_state_machine()
            )
            global tsamis
            tsamis[sm.name] = sm

    uvicorn.run("tsami.server.server:app", host="0.0.0.0", port=8000, reload=False)


if __name__ == "__main__":
    start()
