from flask import Flask, render_template, send_from_directory
from pydantic import BaseModel
import pydot
import requests
import os

app = Flask(__name__)

REST_URL = "http://127.0.0.1:8000"


class TsamisList(BaseModel):
    machines: list[str]


@app.route("/")
def root():
    return render_template("./index.html")


@app.route("/htmx_tsamis")
def get_tsamis():
    response: TsamisList = requests.get(REST_URL + "/allTsamis").json()
    context: dict = {"machines": []}
    for machine in response["machines"]:
        status_rest = requests.get(REST_URL + f"/tsami/{machine}/status").json()
        context["machines"].append(
            {
                "name": machine,
                "status": status_rest["status"],
            }
        )
    return render_template("list_machines.html", context=context)


@app.route("/htmx_dot/<name>")
def render_tsami(name: str):
    stateResponse = requests.get(REST_URL + f"/tsami/{name}/state").json()
    statusResponse = requests.get(REST_URL + f"/tsami/{name}/status").json()
    pathResponse = requests.get(REST_URL + f"/tsami/{name}/path").json()
    state = stateResponse["state"]
    status = statusResponse["status"]
    path = pathResponse["path"]

    img_filename = f"{name}_{state}"

    if not os.path.isfile(os.path.join(app.static_folder, f"{img_filename}.png")):
        # this avoids always requesting the dot and generating but i still need to see
        # how to remove old images so that the static folder doesn't become huge.
        app.logger.info(
            f"Image for {name} and state {state} not generated. Generating."
        )
        dotResponse = requests.get(REST_URL + f"/tsami/{name}/dot").json()
        dot = dotResponse["dot"]
        (graph,) = pydot.graph_from_dot_data(dot)
        path = os.path.join(app.static_folder, f"{img_filename}.png")
        graph.write_png(path)
    else:
        app.logger.info(f"{img_filename}.png found. GG")

    return render_template(
        "sm_viewer.html",
        rest_url=REST_URL,
        name=name,
        img=img_filename,
        path=path,
        status=status,
    )


@app.route("/image/<name>")
def serve_image(name):
    return send_from_directory(f"{name}.png", "./static/")


# I hate this. Completely unnecessary with htmx BUT there are issues when doing
# requests to external third party APIs because htmx headers. This is described in
# this issue https://github.com/bigskysoftware/htmx/issues/779
# I've tried the sugestion of hx-request={noHeaders = true} but i couldn't get it to
# work. These two functions are the workaround as requests sends normal headers.
@app.route("/stop/<name>")
def stop_machine(name: str):
    requests.get(REST_URL + f"/tsami/{name}/stop")
    return ""


@app.route("/run/<name>")
def run_machine(name: str):
    requests.get(REST_URL + f"/tsami/{name}/run")
    return ""


def start():
    app.run(host="0.0.0.0")
