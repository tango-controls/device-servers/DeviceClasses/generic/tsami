from enum import Enum
from typing import Annotated, List, Union
from pydantic import AfterValidator, BaseModel, TypeAdapter
from tango import AttributeProxy

TangoAttributeReadType = Annotated[
    str, AfterValidator(lambda attr: AttributeProxy(attr))
]


class Variable(BaseModel):
    name: str

    def read(self):
        return self.content

    def write(self, value):
        self.content = value


class IntVariable(Variable):
    content: int


class FloatVariable(Variable):
    content: float


class BoolVariable(Variable):
    content: bool


class TangoVariable(Variable):
    content: TangoAttributeReadType

    def read(self):
        return self.content.read().value

    def write(self, value):
        self.content.write(value)


VariableTypesAdapter = TypeAdapter(
    List[Union[FloatVariable, IntVariable, BoolVariable, TangoVariable]]
)


class SMState(str, Enum):
    STANDBY = "STANDBY"
    ASSIGNING = "ASSIGNING"
    SAFETYCHECK = "SAFETY CHECK"
    SAFETYERROR = "SAFETY ERROR"
    RUNNING = "RUNNING"
    FINISHED = "FINISHED"

    CONNECTIONERROR = "TANGO CONNECTION ERROR"
    ERROR = "ERROR"


def compare(left, operation, right):
    """
    Compares left and right parts of an algebraic boolean expression.

        Args:
            left (number): The left side of the expression
            operation (str): The comparison operator for the expression
            right (number): The right side of the expression
    """
    match operation:
        case "==":
            return left == right
        case "<":
            return left < right
        case ">":
            return left > right
        case "<=":
            return left <= right
        case ">=":
            return left >= right


def textx_instance(textx_model) -> str:
    return textx_model._tx_peg_rule.id


class Endpoint(str, Enum):
    ROOT = "/"

    ALL_TSAMIS = "/allTsamis"
    GENERAL_TSAMI = "/tsami"
    CREATE_TSAMI = f"/createTsami"
    TSAMI = f"{GENERAL_TSAMI}/{{name}}"

    RUN_MACHINE = f"{TSAMI}/run"
    STOP_MACHINE = f"{TSAMI}/stop"

    GET_TSAMI_STATE = f"{TSAMI}/state"
    GET_TSAMI_STATUS = f"{TSAMI}/status"
    GET_TSAMI_DOT = f"{TSAMI}/dot"
    GET_TSAMI_PATH = f"{TSAMI}/path"
    GET_TSAMI_STATE_LIST = f"{TSAMI}/state_list"

    HTMX_ROOT = "/"
    HTMX_GET_TSAMIS = "/htmx_tsamis"
    HTMX_GET_DOT_IMAGE = f"{TSAMI}/htmx_dot"
