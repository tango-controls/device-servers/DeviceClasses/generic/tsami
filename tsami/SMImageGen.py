import textx
import codecs
import sys
from os.path import dirname, join

HEADER = """
digraph textX {
fontname = "Bitstream Vera Sans"
fontsize = 8
node[
    shape=record,
    style=rounded
]
edge[dir=black,arrowtail=empty]
"""


def codition_to_str(condition_model):
    match condition_model._tx_peg_rule.id:
        case "Delay":
            return f"delay {condition_model.time}"
        case "Leaf":
            if condition_model.variable is not None:
                return condition_model.variable.name
            else:
                return str(condition_model.x)
        case "Qatom":
            left = codition_to_str(condition_model.left)
            right = codition_to_str(condition_model.right)
            return f"{left} {condition_model.operation} {right}"
        case "AndBoolExp":
            left = codition_to_str(condition_model.left)
            right = None
            if condition_model.right:
                right = codition_to_str(condition_model.right)
            if right:
                return f"{left} and {right}"
            else:
                return left
        case "OrBoolExp":
            if condition_model.delay:
                return codition_to_str(condition_model.delay)
            left = codition_to_str(condition_model.left)
            right = None
            if condition_model.right:
                right = codition_to_str(condition_model.right)
            if right:
                return f"{left} or {right}"
            else:
                return left


def tsm_to_dot(model, current_state=None):
    dot_str = HEADER

    for state in model.states:
        if state.name == current_state:
            dot_str += (
                f'{state.name}[label="{state.name}", color="red", fontcolor="red"]\n'
            )
        else:
            dot_str += f'{state.name}[label="{state.name}"]\n'

        for transition in state.exits:
            dot_str += '{} -> {} [label="{}"]\n'.format(
                state.name,
                transition.to_state.name,
                codition_to_str(transition.condition),
            )

    dot_str += "\n}\n"
    return dot_str


if __name__ == "__main__":
    this_folder = dirname(__file__)
    # if len(sys.argv) != 2:
    #     print('Usage: {}<model>\n'.format(sys.argv[0]))
    #     quit()

    model_name = sys.argv[1]
    try:
        current_state = sys.argv[2]
    except IndexError:
        current_state = None
    meta = textx.metamodel_from_file(join(this_folder, "tsm_grammar.tx"))
    model = meta.model_from_file(model_name)
    with codecs.open("{}.dot".format(model_name), "w", encoding="utf-8") as f:
        f.write(tsm_to_dot(model, current_state=current_state))
