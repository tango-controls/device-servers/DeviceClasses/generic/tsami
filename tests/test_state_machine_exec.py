from time import sleep
from typing_extensions import IntVar
from tsami.SMBuilder import StateMachineBuilder
from tsami.SMUtils import FloatVariable, IntVariable, SMState, TangoVariable
import pytest
import os
from tango import DeviceProxy

#########################################
## TESTS FOR start.tsm
#########################################
TSMS_PATH = ""
if "TSAMI_TEST" in os.environ:
    TSMS_PATH = os.environ["TSAMI_TEST"]
else:
    raise EnvironmentError(
        "No path to find test tsamis is found. Define env var TSAMI_TEST"
    )


@pytest.fixture
def sm():
    return (
        StateMachineBuilder()
        .set_model_path(f"{TSMS_PATH}/start.tsm")
        .generate_state_machine()
    )


@pytest.fixture
def dev():
    dev = DeviceProxy("fakeds/tests/1")
    dev.Init()
    return dev


def test_create(sm):
    info = {
        "path": sm.get_path(),
        "name": sm.name,
        "states": sm.states,
    }

    answer = {
        "path": f"{TSMS_PATH}/start.tsm",
        "name": "vaccum_minerva",
        "states": [
            "vented_system",
            "state1to2_actions",
            "state_2",
            "state2to3_check",
            "state_3",
            "state_4",
            "check_pnv03_state4",
            "state_5",
            "operation_mode",
            "state_error_1",
        ],
    }
    assert info == answer


def test_variable_creation(sm):
    sm.assign_variables_from_definitions()
    vars = {
        "FRG02_1": TangoVariable(
            name="FRG02_1", content="testds/eps/1/X25A0203CH21_VCPEN_EH0103_DI"
        ),
        "FRG02_2": TangoVariable(
            name="FRG02_2", content="testds/eps/1/X25A0203CH22_VCPEN_EH0103_DI"
        ),
        "FRG02_3": TangoVariable(
            name="FRG02_3", content="testds/eps/1/X25A0203CH23_VCPEN_EH0103_DI"
        ),
        "FRG03_1": TangoVariable(
            name="FRG03_1", content="testds/eps/1/X25A0203CH31_VCPEN_EH0104_DI"
        ),
        "TMP02": TangoVariable(name="TMP02", content="testds/eps/1/VCTMPCT_EH0102"),
        "TMP03": TangoVariable(name="TMP03", content="testds/eps/1/VCTMPCT_EH0103"),
        "PNV02": TangoVariable(name="PNV02", content="testds/eps/1/VCPNV_EH0102"),
        "PNV03": TangoVariable(name="PNV03", content="testds/eps/1/VCPNV_EH0103"),
        "PNV04": TangoVariable(name="PNV04", content="testds/eps/1/VCPNV_EH0104"),
        "PNV05": TangoVariable(name="PNV05", content="testds/eps/1/VCPNV_EH0105"),
        "PNV06": TangoVariable(name="PNV06", content="testds/eps/1/VCPNV_EH0106"),
        "PNV07": TangoVariable(name="PNV07", content="testds/eps/1/VCPNV_EH0107"),
        "PDButton": TangoVariable(name="PDButton", content="testds/eps/1/PDButton"),
        "PNVVenting": TangoVariable(
            name="PNVVenting", content="testds/eps/1/VCVALVE_EH0101"
        ),
        "PrimaryPump": TangoVariable(
            name="PrimaryPump", content="testds/eps/1/VCPP_EH0101"
        ),
        "test_float": FloatVariable(name="test_float", content=1234.34),
        "test_int": IntVariable(name="test_int", content=54),
    }
    for name, variable in vars.items():
        assert sm.variables[name].name == variable.name
        assert sm.variables[name].read() == variable.read()


def test_safety_check(sm, dev):
    # Break safe start for minerva test and expect exception
    sm.assign_variables_from_definitions()
    dev.write_attribute("VCPNV_EH0102", 1)  # PNV02
    with pytest.raises(Exception) as e_info:
        sm.run_safety()
    assert sm.status == SMState.SAFETYERROR


def test_state_change(sm, dev):
    assert sm.status == SMState.STANDBY
    sm.run()
    sleep(2)
    assert sm.status == SMState.RUNNING
    assert sm.get_current_state() == "vented_system"
    dev.write_attribute("PDButton", 1)
    sleep(0.5)
    assert sm.status == SMState.RUNNING
    assert sm.get_current_state() == "state1to2_actions"
    sleep(5)
    assert (
        dev.read_attribute("VCVALVE_EH0101").value == 0
        and dev.read_attribute("VCPP_EH0101").value == 1
    )
    assert sm.status == SMState.RUNNING
    assert sm.get_current_state() == "state_2"
    sm.stop()


def test_stop(sm, dev):
    assert sm.status == SMState.STANDBY
    sm.run()
    sleep(2)
    assert sm.status == SMState.RUNNING
    assert sm.get_current_state() == "vented_system"
    dev.write_attribute("PDButton", 1)
    sleep(0.5)
    assert sm.status == SMState.RUNNING
    assert sm.get_current_state() == "state1to2_actions"
    sm.stop()
    assert sm.status == SMState.STANDBY
    assert sm.get_current_state() == None
