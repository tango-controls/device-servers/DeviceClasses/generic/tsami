from tango.server import run, Device, attribute
import time


class FakeDS(Device):
    def init_device(self):
        Device.init_device(self)
        self._X25A0203CH23_VCPEN_EH0103_DI = 0
        self._X25A0203CH21_VCPEN_EH0103_DI = 0
        self._X25A0203CH22_VCPEN_EH0103_DI = 1
        self._X25A0203CH31_VCPEN_EH0104_DI = 1
        self._VCPNV_EH0102 = 0
        self._VCPNV_EH0103 = 0
        self._VCPNV_EH0104 = 0
        self._VCPNV_EH0105 = 1
        self._VCPNV_EH0106 = 1
        self._VCPNV_EH0107 = 0
        self._VCTMPCT_EH0102 = 0
        self._VCTMPCT_EH0103 = 1
        self._VCVALVE_EH0101 = 1
        self._CTPB_EH0101_IO_SWGR_DI = 0
        self._VCPP_EH0101 = 0
        self._PDButton = 0

    @attribute(dtype=int)
    def PDButton(self):
        val = self._PDButton
        self._PDButton = 0
        return val

    @PDButton.write
    def PDButton(self, value):
        self._PDButton = value

    @attribute(dtype=int)
    def VCTMPCT_EH0103(self):
        return self._VCTMPCT_EH0103

    @VCTMPCT_EH0103.write
    def VCTMPCT_EH0103(self, value):
        self._VCTMPCT_EH0103 = value

    @attribute(dtype=int)
    def VCPP_EH0101(self):
        return self._VCPP_EH0101

    @VCPP_EH0101.write
    def VCPP_EH0101(self, value):
        self._VCPP_EH0101 = value

    @attribute(dtype=int)
    def VCVALVE_EH0101(self):
        return self._VCVALVE_EH0101

    @VCVALVE_EH0101.write
    def VCVALVE_EH0101(self, value):
        self._VCVALVE_EH0101 = value

    @attribute(dtype=int)
    def VCTMPCT_EH0102(self):
        return self._VCTMPCT_EH0102

    @VCTMPCT_EH0102.write
    def VCTMPCT_EH0102(self, value):
        self._VCTMPCT_EH0102 = value

    @attribute(dtype=int)
    def X25A0203CH21_VCPEN_EH0103_DI(self):
        return self._X25A0203CH21_VCPEN_EH0103_DI

    @X25A0203CH21_VCPEN_EH0103_DI.write
    def X25A0203CH21_VCPEN_EH0103_DI(self, value):
        self._X25A0203CH21_VCPEN_EH0103_DI = value

    @attribute(dtype=int)
    def X25A0203CH22_VCPEN_EH0103_DI(self):
        return self._X25A0203CH22_VCPEN_EH0103_DI

    @X25A0203CH22_VCPEN_EH0103_DI.write
    def X25A0203CH22_VCPEN_EH0103_DI(self, value):
        self._X25A0203CH22_VCPEN_EH0103_DI = value

    @attribute(dtype=int)
    def X25A0203CH23_VCPEN_EH0103_DI(self):
        return self._X25A0203CH23_VCPEN_EH0103_DI

    @X25A0203CH23_VCPEN_EH0103_DI.write
    def X25A0203CH23_VCPEN_EH0103_DI(self, value):
        self._X25A0203CH23_VCPEN_EH0103_DI = value

    @attribute(dtype=int)
    def X25A0203CH31_VCPEN_EH0104_DI(self):
        return self._X25A0203CH31_VCPEN_EH0104_DI

    @X25A0203CH31_VCPEN_EH0104_DI.write
    def X25A0203CH31_VCPEN_EH0104_DI(self, value):
        self._X25A0203CH31_VCPEN_EH0104_DI = value

    @attribute(dtype=int)
    def VCPNV_EH0102(self):
        return self._VCPNV_EH0102

    @attribute(dtype=int)
    def VCPNV_EH0102(self):
        return self._VCPNV_EH0102

    @VCPNV_EH0102.write
    def VCPNV_EH0102(self, value):
        self._VCPNV_EH0102 = value

    @attribute(dtype=int)
    def VCPNV_EH0103(self):
        return self._VCPNV_EH0103

    @VCPNV_EH0103.write
    def VCPNV_EH0103(self, value):
        self._VCPNV_EH0103 = value

    @attribute(dtype=int)
    def VCPNV_EH0104(self):
        return self._VCPNV_EH0104

    @VCPNV_EH0104.write
    def VCPNV_EH0104(self, value):
        self._VCPNV_EH0104 = value

    @attribute(dtype=int)
    def VCPNV_EH0105(self):
        return self._VCPNV_EH0105

    @VCPNV_EH0105.write
    def VCPNV_EH0105(self, value):
        self._VCPNV_EH0105 = value

    @attribute(dtype=int)
    def VCPNV_EH0106(self):
        return self._VCPNV_EH0106

    @VCPNV_EH0106.write
    def VCPNV_EH0106(self, value):
        self._VCPNV_EH0106 = value

    @attribute(dtype=int)
    def VCPNV_EH0107(self):
        return self._VCPNV_EH0107

    @VCPNV_EH0107.write
    def VCPNV_EH0107(self, value):
        self._VCPNV_EH0107 = value


if __name__ == "__main__":
    run((FakeDS,))
